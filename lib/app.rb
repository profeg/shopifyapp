require 'sinatra/shopify-sinatra-app'
require 'tilt/erb'
require 'pry'
require 'carrierwave'

require './lib/uploaders/image_uploader'

class SinatraApp < Sinatra::Base
  register Sinatra::Shopify

  set :scope, 'write_products, write_customers, write_content'

  get '/' do
    shopify_session do
      @products = ShopifyAPI::Product.find(:all)
      erb :home
    end
  end

  get '/customer/:customer/products' do
    headers 'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => ['OPTIONS', 'GET', 'POST']
    session["shopify"] = {:shop=>"profeg.myshopify.com", :token=>"c72b8a71ff9c8e468f5c2976bc2631b7"}
    shopify_session do
      @customer = ShopifyAPI::Customer.find(params[:customer])
      if @customer
        products_metafields = @customer.metafields.select { |m| m.namespace = 'products' }
        products_ids = products_metafields.collect {|mf| mf.key.to_i }.uniq.compact
        @products = ShopifyAPI::Product.find(:all).select { |p| products_ids.include? p.id }
        erb :products_list, layout: false
      end
    end
  end

  get '/customer/:customer/product/:product' do
    headers 'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => ['OPTIONS', 'GET', 'POST']
    session["shopify"] = {:shop=>"profeg.myshopify.com", :token=>"c72b8a71ff9c8e468f5c2976bc2631b7"}
    shopify_session do
      @product = ShopifyAPI::Product.find params[:product]
      @customer = ShopifyAPI::Customer.find(params[:customer])
      erb :form, layout: false
    end
  end

  post '/product' do
    headers 'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => ['OPTIONS', 'GET', 'POST']
    session["shopify"] = {:shop=>"profeg.myshopify.com", :token=>"c72b8a71ff9c8e468f5c2976bc2631b7"}
    shopify_session do
      collection = ShopifyAPI::CustomCollection.where(title: 'Home page').first
      product = params[:product].present? ? ShopifyAPI::Product.find(params[:product]) : ShopifyAPI::Product.new()
      product.update_attributes params.slice('title', 'body_html')
      if product.save
        # image
        # image = ShopifyAPI::Image.new(:product_id => @product.id, :position => 1)
        # collection
        ShopifyAPI::Collect.create(product_id: product.id, collection_id: collection.id)
        # metafileds
        customer = ShopifyAPI::Customer.find(params['customer'])
        metafield = ShopifyAPI::Metafield.new({
            key: product.id,
            namespace: 'products',
            value: 'id',
            value_type: 'string'
        })
        customer.add_metafield(metafield)
        customer.save
      end
      redirect 'https://profeg.myshopify.com'
    end
  end

  post '/customer/:customer/product/:product/delete' do
    headers 'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => ['OPTIONS', 'DELETE']
    session["shopify"] = {:shop=>"profeg.myshopify.com", :token=>"c72b8a71ff9c8e468f5c2976bc2631b7"}
    shopify_session do
      collection = ShopifyAPI::CustomCollection.where(title: 'Home page').first
      product = ShopifyAPI::Product.find(params[:product])
      product.remove_from_collection(collection)
      product.save
      customer = ShopifyAPI::Customer.find(params[:customer])
      metafield = customer.metafields.find { |mf| mf.key.to_i == product.id}
      metafield.destroy
    end
    "ok".to_json
  end

  post '/uninstall' do
    webhook_session do |params|
      current_shop.destroy
    end
  end

  private
  def after_shopify_auth
  end
end
